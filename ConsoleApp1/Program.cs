﻿using System;
class PayDeneme
{
    private string Client;
    private string ReferenceID;
    private string PlatformID;
    private string BankID;
    private string TypeID;
    private string CurrencyCode;
    private string Amount;
    private string SubmitDateTime;
    private string RedirectUrl;
    private string NotificationUrl;
    private string IP;
    private string Remark;
    private string PlayerID;
    private string Sign;
    public class Product
    {
        public string Client
        {
            get
            {
                return Client;
            }
            set
            {
                Client = value;
            }
        }

        public string ReferenceID
        {
            get
            {
                return ReferenceID;
            }
            set
            {
                ReferenceID = value;
            }
        }

        public int PlatformID
        {
            get
            {
                return PlatformID;
            }
            set
            {
                PlatformID = value;
            }
        }
        public int BankID
        {
            get
            {
                return BankID;
            }
            set
            {
                BankID = value;
            }
        }

        public int TypeID
        {
            get
            {
                return TypeID;
            }
            set
            {
                TypeID = value;
            }
        }
        public string CurrencyCode
        {
            get
            {
                return CurrencyCode;
            }
            set
            {
                CurrencyCode = value;
            }
        }
        public decimal Amount
        {
            get
            {
                return Amount;
            }
            set
            {
                Amount = value;
            }
        }
        public string SubmitDateTime
        {
            get
            {
                return SubmitDateTime;
            }
            set
            {
                SubmitDateTime = value;
            }
        }
        public string RedirectUrl
        {
            get
            {
                return RedirectUrl;
            }
            set
            {
                RedirectUrl = value;
            }
        }
        public string NotificationUrl
        {
            get
            {
                return NotificationUrl;
            }
            set
            {
                NotificationUrl = value;
            }
        }
        public string IP
        {
            get
            {
                return IP;
            }
            set
            {
                IP = value;
            }
        }
        public string Remark
        {
            get
            {
                return Remark;
            }
            set
            {
                Remark = value;
            }
        }
        public string PlayerID
        {
            get
            {
                return PlayerID;
            }
            set
            {
                PlayerID = value;
            }
        }
        public string Sign
        {
            get
            {
                return Sign;
            }
            set
            {
                Sign = value;
            }
        }
    }
    class Program

    {
        static void Main(string[] args)
        {
            PayDeneme V1 = new PayDeneme();
            
            Console.Write("Client: ");
            V1.Client = Console.ReadLine();

            Console.Write("ReferenceID: ");
            V1.ReferenceID = Console.ReadLine();

            Console.Write("PlatformID: ");
            V1.PlatformID = Console.ReadLine();

            Console.WriteLine("BankaID : <B2C BankID/Alipay BankID>");
            V1.BankID = Console.ReadLine();

            Console.Write("TypeID: ");
            V1.TypeID = Console.ReadLine();

            Console.Write("CurrencyCode: ");
            V1.CurrencyCode = Console.ReadLine();

            Console.Write("Amount: ");
            V1.Amount = Console.ReadLine();

            Console.Write("SubmitDateTime: ");
            V1.SubmitDateTime = Console.ReadLine();

            Console.Write("RedirectUrl: ");
            V1.RedirectUrl = Console.ReadLine();

            Console.Write("NotificationUrl: ");
            V1.NotificationUrl = Console.ReadLine();

            Console.Write("IP: ");
            V1.IP = Console.ReadLine();

            Console.Write("Remark: ");
            V1.Remark = Console.ReadLine();

            Console.Write("PlayerID: ");
            V1.PlayerID = Console.ReadLine();

            Console.Write("Sign: ");
            V1.Sign = Console.ReadLine();

            Console.WriteLine("Client:{0} - ReferenceID:{1} - PlatformID:{2} - BankID:{3} - TypeID:{4} - CurrencyCode:{5} - Amount:{6} - SubmitDateTime:{7} - RedirectUrl:{8} - NotificationUrl:{9} - IP:{10} - Remark:{11} - PlayerID:{12} - Sign:{13}", V1.Client, V1.ReferenceID, V1.PlatformID, V1.BankID, V1.TypeID, V1.CurrencyCode, V1.Amount, V1.SubmitDateTime, V1.RedirectUrl, V1.NotificationUrl, V1.IP, V1.Remark, V1.PlayerID, V1.Sign);

        }
    }
}

